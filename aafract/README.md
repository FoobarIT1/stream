[[_TOC_]]

# aafract

A multi-engine fractal viewer written in modern C++.

## Dependencies

	sudo apt install make cmake g++ libsdl2-dev libsdl2-ttf-dev \
	  libsdl2-gfx-dev cppcheck libaa1-dev libgtest-dev

## Building

Un visualizer de pathfinder en C++/SDL2

	mkdir build
	cd build/
	cmake ..
	make
	./aafract
	
## Doxygen documentation

	cd build/
	cmake ..
	doxygen
	[browser] html/index.html
	
