#
# Search and set aalib INCLUDES ans CFLAGS from aalib-config binary.
#

# Search for the `aalib-config` binary shuipped with the libaa1-dev.
# Issue an error but do not interrupt cmake if not found
find_program(aalibconfig_EXECUTABLE NAMES aalib-config)
#SET(aalib_INCLUDES "aalib-config --cflags")
if("${aalibconfig_EXECUTABLE}" STREQUAL "aalibconfig_EXECUTABLE-NOTFOUND")
  message(SEND_ERROR "Can't find 'aalib-config'. Please install libaa1-dev")
endif()

# Call `aalib-config` binary to know CFLAGS compiler
execute_process(COMMAND ${aalibconfig_EXECUTABLE} --cflags
                WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
                RESULT_VARIABLE aalib_INCLUDES_result
                OUTPUT_VARIABLE aalib_INCLUDES
		OUTPUT_STRIP_TRAILING_WHITESPACE)

# Call `aalib-config` binary to know LIBS linker flags
execute_process(COMMAND ${aalibconfig_EXECUTABLE} --libs
                WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
                RESULT_VARIABLE aalib_LIBS_result
                OUTPUT_VARIABLE aalib_LIBS
		OUTPUT_STRIP_TRAILING_WHITESPACE)

