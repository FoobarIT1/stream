#ifndef __MAIN_WINDOW_HPP__
#define __MAIN_WINDOW_HPP__

// Forward declarations
struct SDL_Window;
// End of forward declarations

/** The main (and only) window opened by this application
  *
  */
class MainWindow
{
public:
  MainWindow();
  ~MainWindow();

  int run();

private:
  SDL_Window* window;  //!< Declare a pointer to the to-be-created SDL win
};

#endif // !__MAIN_WINDOW_HPP__
