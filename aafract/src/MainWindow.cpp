#include "MainWindow.hpp"

#include <SDL2/SDL.h>

#include <iostream>  // USES std::cin
#include <sstream>
#include <string>

/** Construct the SDL main window
  *
  * May thow an exception in case of SDL initialization eror. In this case,
  * the status code can be set to 134.
  *
  */

MainWindow::MainWindow():
  window(nullptr)
{
  SDL_Init(SDL_INIT_VIDEO);              // Initialize SDL2

  // Create an application window with the following settings:
  window = SDL_CreateWindow("An SDL2 window",                  // window title
		    SDL_WINDOWPOS_UNDEFINED,           // initial x position
		    SDL_WINDOWPOS_UNDEFINED,           // initial y position
		    640,                               // width, in pixels
		    480,                               // height, in pixels
        SDL_WINDOW_OPENGL                  // flags - see below
			    );

  // Check that the window was successfully created
  if (window == nullptr)
    {
      std::ostringstream oss;
      oss << "Could not create window: " << SDL_GetError();

      // In the case that the window could not be made...
      std::cerr << "Could not create window: " << SDL_GetError() << std::endl;
      throw std::runtime_error(oss.str());
    }
}

/** Destroy the SDL window and exit SDL
  *
  */
MainWindow::~MainWindow()
{
  // Close and destroy the window
  SDL_DestroyWindow(window);

  // Clean up
  SDL_Quit();
}

/** Enter the game loop
  *
  * \return The status code of the application. 0 for success, any other value
  *         for an error.
  *
  */
int
MainWindow::run()
{
  std::cout << "Hit a key in the terminal to exit..." << std::endl;

  // The window is open: could enter program loop here (see SDL_PollEvent())
  // Implement FPS:
  bool running = true;
  while (running) {

  	Uint64 start = SDL_GetPerformanceCounter();


  	// Do event loop

  	// Do physics loop
    SDL_Delay(120);
  	// Do rendering loop

  	Uint64 end = SDL_GetPerformanceCounter();
    // Adding box in right corner of SQL window (Need SDL_ttf).
  	float elapsed = (end - start) / (float)SDL_GetPerformanceFrequency();
  	std::cout << "Current FPS: " << std::to_string(1.0f / elapsed) << std::endl;

    SDL_Event event;
    if (SDL_PollEvent(&event) == 1)
      {
        if (event.type == SDL_KEYDOWN )
          running=false;

          // Fix: This condition instant close program when running statut change.
          if (!running) {
            SDL_DestroyWindow(window);
            SDL_Quit();
          }

          //Check if running pass false.
          std::cout << " Running Status: " << running << std::endl;
      }

  }

  return 0;
}
