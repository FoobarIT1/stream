/** \file main.cpp
  * Main entry of the aafract application code base.
  *
  */

#include <iostream>

#include "MainWindow.hpp"

/*
#include <aalib.h>
aa_context *context;

...

  std::cout << "aafract :" << std::endl;

  context = aa_autoinit(&aa_defparams);
  if(context == NULL) {
    fprintf(stderr,"Cannot initialize AA-lib. Sorry\n");
    exit(1);
  }
  //  ...
  aa_close(context);
*/


/** Main entry of the program
  *
  * \return Statuc code of the program execution.
  *
  */
int
main(int /*argc*/, char** /*argv*/)
{
  MainWindow mw;
  return mw.run();
}
