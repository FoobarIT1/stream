#include "list.h"

#include <stdlib.h>

List*
list_create(void* data)
{
  List* l = malloc(sizeof(List));
  l->data = data;
  l->next = NULL;
  return l;
}

void
list_append(List* l, void* data)
{
  List* it = l;
  while (it->next)
    it = it->next;

  it->next = list_create(data);
}

size_t
list_len(List* l)
{
  size_t len = 0;
  List* it = l;
  while (it)
    {
      ++len;
      it = it->next;
    }
  return len;
}

void
list_free(List** l)
{
  List* bc;
  List* it = *l;
  while (it)
    {
      bc = it;
      it = it->next;
      free(bc);
    }
  *l=NULL;
}

