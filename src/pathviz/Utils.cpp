#include "Utils.hpp"

#include <cmath>      // USES sqrt()
#include <stdexcept> // USES runtime_error
	

using namespace std;

double
distance(int x1, int y1, int x2, int y2)
{
  int dx = x2 - x1;
  int dy = y2 - y1;
  return sqrt((double) dx*dx + dy*dy);
}

/** Check if the given cmd (second argument doesn't fail */
void
check_sdl_cmd(const string& message, const string& command,
	      const string& file, int line, 
	      int return_value)
{
  if (return_value != 0)
    {
      string loc = "[" + file + '@' + to_string(line) + "]: \n";
      throw runtime_error(loc + "    `" + command + "`: '" + message +
			  "' failed!");
    }
}
