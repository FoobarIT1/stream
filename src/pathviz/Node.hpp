#ifndef __NODE_HPP__
#define __NODE_HPP__

/** A graph node
  *
  * x and y are its position on the screen.
  *
  */
class Node
{
public:
  Node();

  int x; //!< The x position
  int y; //!< The y position
};

#endif // !__NODE_HPP__
