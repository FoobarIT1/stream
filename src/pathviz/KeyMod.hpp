#ifndef __KEY_MOD_HPP__
#define __KEY_MOD_HPP__

#include <SDL2/SDL.h>

// Forward declarations
class Drawer;
// End of forward declarations

/** KeyMod handler beffore drawing
  *
  */
class KeyMod
{
public:
  KeyMod();

  void injectKeyUp(const SDL_Keysym&);
  void injectKeyDown(const SDL_Keysym&);

  void injectModUp(const SDL_Keymod&);
  void injectModDown(const SDL_Keymod&);

  void draw(SDL_Renderer*, Drawer*);
private:
  bool  showKey;  //!< Should we print key
  SDL_Keysym key; //!< Shown if show_key
  SDL_Keymod mod; //!< Shown if not KMOD_NONE
};
#endif // !__KEY_MOD_HPP__
