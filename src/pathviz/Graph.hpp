#ifndef __GRAPH_HPP__
#define __GRAPH_HPP__

#include <vector>   // I use vector to use direct acces (i.e v[0])

// Forward declarations
class Node;
// End of forward declarations

typedef struct
{
  int a, b; // The nodes index in the Graph vector
  unsigned int weight; // The weight associated with this egde
}Edge;

/** A datastructure composed of Node and Edges.
  * 
  * Here is a weighted graph. Node are placed on the screen (X, Y).
  * Each edge has a weight (thetwo node (vertices) distance).
  *
  */
class Graph
{
public:
  Graph();

  int randomize(int size=5);
  
  const std::vector<Node*>&  getNodes() const;
  const std::vector<Edge*>& getEdges() const;

protected:
  void addNode(int, int);

  bool isEdgeValid(int);
  bool addEdge(int, int);

  int computeClosestNode(int, int) const;
  bool intersects(int, int);
  
private:
  std::vector<Node*> nodes; //!< The vector of Node(s).
  std::vector<Edge*> edges; //!< The vector of Edge(s).

  /// Minimal distance between two nodes
  int minimalDistance;
};

#endif //! __GRAPH_HPP__
