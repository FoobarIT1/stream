#ifndef __ENGINE_HPP__
#define __ENGINE_HPP__

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <list>
#include <string>

#include "Utils.hpp"   // USES distance()

#include "Log.hpp"
#include "Graph.hpp"
#include "KeyMod.hpp"

// Forward declarations
class Text;
// End of forward declarations


/** All the variables needed to print temporary message
  *
  */
typedef struct {
  std::string message;   //!< Message to be printed
  bool   show;           //!< Print ?
  int    remaining_time; //!< If shown, 
  Uint32 total_time;     //!< Total time
  Text*  text;           //!< The created text
} TemporaryMessage;

/** Used to change node number
  *
  */
struct NodeNumbers{
  std::vector<int> numbers; // The available node numbers
  int it;

  NodeNumbers():
    numbers{5, 10, 15, 25, 50},
    it(0)
  {
  }
  int number(void)    { return numbers[it];  }
  void next(void)     {
    if (++it > numbers.size() - 1)
      it = numbers.size() - 1;

    debug();
  }
  void previous(void) {
    if (--it < 0)
      it = 0;
	
    debug();
  }
  void debug(void) { L("Number is now " << number()); }
};

/** Everything used to compute and draw FPS
  *
  */
struct Fps
{
  std::string current_fps;
  Uint32 update_delta;      // Update each x ms
  Uint32 last_update_delta; // Increment with update delta

  Uint32 lasttime;
  Uint32 frames;
  Text*  text;           //!< The created text
};

/** The aplication engine, handling drawing and events
  *
  * This is the main application class. Its lifetime is the application's one.
  *
  */
class Engine
{
public:
  Engine();
  ~Engine();

  void initSDL(void);
  int run(void);

protected:
  void printKeyInfo( SDL_KeyboardEvent*);
  void PrintModifiers(Uint16);
  void drawMouseCursor(void);

  void addInstructions(int, const std::string&, const std::string&);
  void computeClosestNode(void);

  void addTemporaryMessage(const std::string&);
  void printTemporaryMessage(Uint32);
  void updateFps(Uint32);
  
private:
  //  Pointers
  SDL_Window*   window;          //!< The main and only SDL window
  SDL_Renderer* windowRenderer;  //!< The full window renderer
  TTF_Font*     font;            //!< The global TTF font

  // States
  int          width;            //!< The window width in pixels
  int          height;           //!< The window height in pixels
  const char*  title;            //!< The window title
  bool         running;          //!< Is the application running ?
  SDL_Color    textColor;        //!< Basic text color
  SDL_Color    greenColor;       //!< Basic text color in green xD
  bool         hwrenderer;   //!< Are we using a Hardware accelerated renderer
  
  // Lists ansd structs
  std::list<Text*>  texts;       //!< All Text objects, written all frame

  Graph            g;            //!< The represented graph
  Uint32           previousTicks; //!< Used to compute delta

  ClosestNode      closest;      //!< Keep the closest node from the mouse
  TemporaryMessage tempMsg;      //!< Temporary message
  NodeNumbers      nodeNumbers;  //!< Compute number of node (actual and...)
  int              countedFrames;//!< Used to compute FPS
  KeyMod           keyMod;       //!< KeyMod handler
  Fps              fps;
  
};

#endif // !__ENGINE_HPP__
