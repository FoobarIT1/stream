#include "KeyMod.hpp"

#include "Drawer.hpp"

#include <string>

using namespace std;

KeyMod::KeyMod():
  showKey(false)
{

}

void
KeyMod::injectKeyUp(const SDL_Keysym& k)
{
  showKey = false;
}

void
KeyMod::injectKeyDown(const SDL_Keysym& k)
{
  key = k;
  showKey = true;
}

void
KeyMod::injectModUp(const SDL_Keymod& m)
{
  mod = KMOD_NONE;
}

void
KeyMod::injectModDown(const SDL_Keymod& m)
{
  mod = m;
}

void
KeyMod::draw(SDL_Renderer*r, Drawer* d)
{
  string s(SDL_GetKeyName(key.sym));

  if (showKey)
    d->printKey(r, s);

  if (mod != KMOD_NONE)
    d->printMod(r, "MOD");
}
