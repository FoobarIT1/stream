#ifndef __EXCEPTION_HPP__
#define __EXCEPTION_HPP__

#include <iostream>

// get meta information from pre-processor
// replace with __FILENAME__ to get a fixed cmake version without full path
#define __CUSTOM_THROW(ec, MESSAGE)					\
  throw Exception(ec, __FILENAME__, __FUNCTION__, __LINE__, MESSAGE)


#define ThrowExc(ec, MESSAGE) __CUSTOM_THROW(ec, MESSAGE)


class Exception : public std::exception {
public: 
  Exception(int errorCode, const std::string &message) noexcept;
  Exception(int errorCode, const char *file, const char *function,
	    unsigned int line, const std::string &message) noexcept;
  virtual ~Exception() = default;
  virtual const char* what() const noexcept override;
  
private:
  int errorCode;
  std::string m_message;
};

#endif // !__EXCEPTION_HPP__
