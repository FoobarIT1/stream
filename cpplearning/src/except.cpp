
#include <stdexcept>
	

#include "exception.hpp"

void division(int, int);

int
main()
{
  try
    {
      //throw std::runtime_error("message");
      //throw Exception(-EINVAL, "Invalid parameter.");
      //      ThrowExc(-EINVAL, "Invalid parameter.");

      division(5,0);
      division(5,15);
    }
  catch (const Exception &e)
    {
      std::cout << "Custom exception catched : " << e.what() << std::endl;
      return 1;
    }

  catch (const std::runtime_error &e)
    {
      std::cout << "Custom exception catched (2): " << e.what() << std::endl;
      return 2;
    }
  
  return 0;
}

void
division(int a, int b)
{
  if (b==0)
    ThrowExc(-EINVAL, "Division by 0");


  if (b>100)
    throw std::runtime_error("b is too big");
}

