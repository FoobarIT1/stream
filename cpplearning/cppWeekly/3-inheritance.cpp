#include <iostream>
#include <memory>

struct Base
{
  virtual void do_thing() = 0; // pure virtual function
  virtual ~Base() = default;

  /*
    Base() = default;
    Base(const Base&) = default;
    Base(Base&&) = default;
    Base& operator=(const Base&) = default;
    Base& operator=(Base&&) = default;

   */
  
  int data{};          // 1. Unitialized member

  // see https://en.cppreference.com/w/cpp/language/aggregate_initialization
};

struct Derived : public Base
{
  // overload 
  virtual void do_thing(int i){ } // 2. Hide instead of overriding
  // void do_thing() override {}
};


int
main()
{
  Base *b= new Derived();
  b->do_thing();
  // 3. Memory leak
  // delete b; // to fix

  // std::unique_ptr<Base> b = std::unique_ptr<Base>(new Derived());
  // or static allocation : Derived d; d.do_thing();
  
  return 0;
}
