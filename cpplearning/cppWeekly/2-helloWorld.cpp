#include <iostream>

int
main()
{
  // Avoid using 'using namespace', prefer 'using' or in place 'srd::cout'
  using std::cout;
  using std::endl;
  cout <<"Hello world!" << endl; // Jason also replace endl with in-string "\n"

  return 0;
}
