# The project

Follow https://hackingcpp.com/cpp/educational_videos.html on stream.

# The videos tutorials

## CppWeekly:Learning Modern C++ Mini Series

### [1] The tools 

https://www.youtube.com/watch?v=zMrP8heIz3g

Video example uses Win 10 and :
- Visual Studio 2017 Community : it has Boost.Test, GTest and cmake suport;
  Download size : 6.31Gb.
- Install *CLang Power Tools* extension.
- `cppcheck`, a statical analysis tool.

	sudo apt install clang-tidy

### [2] Hello World

https://www.youtube.com/watch?v=juJaaCf_yKc

Many time used to configure VS and cland. We'll use it from the console.
- Even the `using namespace std` is bad because it includes the complete
  namespace. Use `using std::cout;` in function instead.

### [3] Inheritance

https://www.youtube.com/watch?v=43qyUASBeUc

Some inheritance related issues here :
- uninitialized member can be fixed with `int data{}`. see aggregate initializer
  https://en.cppreference.com/w/cpp/language/aggregate_initialization
- use *override specifier* to avoid hiding instead of overidding when using 
  *virtual* keyword.
- Add a default destructor as `virtual ~Base() = default;`.
- Usage of `std::unique_ptr` from memory header.
